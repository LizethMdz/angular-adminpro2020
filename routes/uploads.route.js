/**
 * Busquedas
 * ruta: '/api/uploads/:coleccion/:archivo'
 */

const { Router } = require('express');

// Files
const expressfileUpload = require('express-fileupload');

const { check } = require('express-validator');
const { validarCampos } = require('../middlewares/validar-campos');
const { validarJWT } = require('../middlewares/validar-jwt');

// Controladores
const {
    putFilesByColeccion,
    retornaImagen
} = require('../controllers/uploads.controller');

const router = Router();

// Default options for uploading files
router.use(expressfileUpload());

// Endpoints
router.put('/:tipo/:id', validarJWT, putFilesByColeccion);

router.get('/:tipo/:foto', retornaImagen);

module.exports = router;