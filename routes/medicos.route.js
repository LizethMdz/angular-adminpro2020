/*
    Hospitales
    ruta: '/api/medicos
*/

const { Router } = require('express');
const { check } = require('express-validator');
const { validarCampos } = require('../middlewares/validar-campos');
const { validarJWT } = require('../middlewares/validar-jwt');

// Controladores
const {
    getMedicos,
    getMedicoById,
    createMedico,
    updateMedico,
    deleteMedico
} = require('../controllers/medicos.controller')

const router = Router();

router.get('/', validarJWT, getMedicos);

router.post('/',
    [
        validarJWT,
        check('nombre', 'El nombre del médico es requerido').not().isEmpty(),
        check('hospital', 'Debes ser un id válido').isMongoId(),
        validarCampos
    ],
    createMedico
);

router.put('/:id',
    [
        validarJWT,
        check('nombre', 'El nombre del médico es requerido').not().isEmpty(),
        check('hospital', 'Debes ser un id válido').isMongoId(),
        validarCampos
    ],
    updateMedico
);

router.delete('/:id',
  validarJWT,
  deleteMedico
);

router.get('/:id',
  validarJWT,
  getMedicoById
);





module.exports = router;
