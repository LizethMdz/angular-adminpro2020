// Importacion en variables de entorno
require('dotenv').config();
const path = require('path');
const express = require('express');
const cors = require('cors');
//Importacion de modulos
const { dbConnection } = require('./database/config');
const app = express();

//============= CONFIGURE CORS ===================
app.use(cors());
//============= READ AND PARSE BODY ==============
app.use(express.json());
//============= CONNECT TO DATA BASE =============
dbConnection();
//============= PUBLIC DIR =======================
app.use(express.static('public'));

//============= OLD CODE FORMAT ==================

// respond with "hello world" when a GET request is made to the homepage
// app.get('/', function (req, res) {
//     res.send('hello world');
// });

//============= TEMPORAL USER ==================

// mean_user
// RCMynujnqr5zblwG

//================== REST API ==================

//Routes

app.use('/api/usuarios', require('./routes/usuarios.route'));
app.use('/api/hospitales', require('./routes/hospitales.route'));
app.use('/api/medicos', require('./routes/medicos.route'));
app.use('/api/todo', require('./routes/busquedas.route'));
app.use('/api/upload', require('./routes/uploads.route'));
app.use('/api/login', require('./routes/auth.route'));

// Integración de las rutas al FrontEnd
app.get('*', (req, res) => {
  res.sendFile(path.resolve(__dirname, 'public/index.html'));
});

//============= CORRER SERVIDOR ==================
app.listen(process.env.PORT, () => {
    console.log("Servidor corriendo en el puerto " + process.env.PORT);
});
